﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class showCoins : MonoBehaviour {
	private int totalCoin;
	public Text coins;
	// Use this for initialization
	void Start () {
		if (PlayerPrefs.HasKey ("coins")) {
			totalCoin = PlayerPrefs.GetInt ("coins");
			coins.text = " X " + totalCoin;
		} else {
			coins.text = " X " + 0;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
