﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {
	private LevelHelper levelHelper;
	public int coinValue;
	// Use this for initialization
	void Start () {
		levelHelper = FindObjectOfType<LevelHelper> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "Player"){
			//Destroy (gameObject);
			gameObject.SetActive(false);
			levelHelper.addToCoins (coinValue);
		}
	}
}
