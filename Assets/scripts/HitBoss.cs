﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoss : MonoBehaviour {
	public int damageToGiveBoss;
	private  Boss_1 boss;
	// Use this for initialization
	void Start () {
		boss = FindObjectOfType<Boss_1> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "knife"){
			boss.hitBoss (damageToGiveBoss);
		}
	}
}
