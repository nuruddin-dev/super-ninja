﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLevelWall : MonoBehaviour {
	private BoxCollider2D bossLevelWallCollider;
	// Use this for initialization
	void Start () {
		bossLevelWallCollider = GetComponent<BoxCollider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerExit2D(Collider2D collider){
		if(collider.tag == "Player"){
			bossLevelWallCollider.isTrigger = false;
		}
	}
}
