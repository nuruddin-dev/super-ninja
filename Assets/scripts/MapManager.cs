﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MapManager : MonoBehaviour {
	RectTransform mapPanal;
	GridLayoutGroup map;
	public  int col, row;

	// Use this for initialization
	void Start () {
		mapPanal = gameObject.GetComponent<RectTransform> ();
		map = gameObject.GetComponent<GridLayoutGroup> ();
		map.cellSize = new Vector2 (mapPanal.rect.width/col,mapPanal.rect.height/row);
	}

}
