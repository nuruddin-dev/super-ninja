﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetObject : MonoBehaviour {

	private Vector3 startPosition;
	private Quaternion startRotation;
	private Vector3 startLocalScale;
	private Rigidbody2D objectRigidBody;
	// Use this for initialization
	void Start () {
		startPosition = transform.position;
		startRotation = transform.rotation;
		startLocalScale = transform.localScale;
		if(GetComponent<Rigidbody2D>()!=null){
			objectRigidBody = GetComponent<Rigidbody2D> ();
		}
	}

	// Update is called once per frame
	void Update () {

	}
	public void resetObject(){
		transform.position = startPosition;
		transform.rotation = startRotation;
		transform.localScale = startLocalScale;
		if(objectRigidBody!=null){
			objectRigidBody.velocity = Vector3.zero;
		}
	}
}
