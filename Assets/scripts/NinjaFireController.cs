﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaFireController : MonoBehaviour {
	public float moveSpeed;
	private Rigidbody2D ninjaKnifeRigidbody;
	private PlayerController playerController;
	public GameObject knifeDeathEffact;
	public GameObject knifeKnockEffact;
	public AudioSource enemyDeathByKnifeSound;
	public float deathSelfTime;

	// Use this for initialization
	void Start () {
		ninjaKnifeRigidbody = GetComponent<Rigidbody2D> ();
		playerController = FindObjectOfType<PlayerController> ();
		if (playerController.transform.localScale.x < 0) {
			transform.localScale = new Vector2 (-0.5f,0.5f);
			moveSpeed = -moveSpeed;
		}
		StartCoroutine ("deathSelfCoRoutine");
	}

	// Update is called once per frame
	void Update () {
		ninjaKnifeRigidbody.velocity = new Vector2 (moveSpeed,ninjaKnifeRigidbody.velocity.y);
	}

	void OnTriggerEnter2D(Collider2D collider){
		
		if(collider.tag=="enemy"){
			Instantiate (knifeDeathEffact,collider.gameObject.transform.position,collider.gameObject.transform.rotation);
			if(LevelHelper.soundStatus == 1){
				Helper.AudioPlay (enemyDeathByKnifeSound);
			}
			collider.gameObject.SetActive (false);
			Destroy (gameObject);
		}
		if(collider.tag=="platformAndObject"){
			Instantiate (knifeKnockEffact,gameObject.transform.position,gameObject.transform.rotation);
			if(LevelHelper.soundStatus == 1){
				Helper.AudioPlay (enemyDeathByKnifeSound);
			}
			Destroy (gameObject);
		}

	}
	public IEnumerator deathSelfCoRoutine(){
		yield return new WaitForSeconds(deathSelfTime);
		Destroy (gameObject);
	}
}
