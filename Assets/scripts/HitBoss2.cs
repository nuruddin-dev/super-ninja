﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoss2 : MonoBehaviour {

	public int damageToGiveBoss;
	private  Boss_4 boss;
	// Use this for initialization
	void Start () {
		boss = FindObjectOfType<Boss_4> ();
	}

	// Update is called once per frame
	void Update () {

	}
	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "knife"){
			boss.hitBoss (damageToGiveBoss);
		}
	}
}
