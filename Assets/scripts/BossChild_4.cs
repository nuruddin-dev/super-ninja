﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossChild_4 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "playerFoot" || collider.tag == "knife" || collider.tag == "deadPanel"){
			Destroy (gameObject);
		}
	}
}
