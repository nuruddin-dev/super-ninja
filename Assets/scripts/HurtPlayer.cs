﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayer : MonoBehaviour {
	private LevelHelper levelHelper;
	public int damageToGive;
	// Use this for initialization
	void Start () {
		levelHelper = FindObjectOfType<LevelHelper> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "Player"){
			//levelHelper.respawn ();
			levelHelper.hurtPlayer(damageToGive);
		}
	}
}
