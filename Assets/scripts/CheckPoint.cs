﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {
	private SpriteRenderer checkPointSpriteRenderer;
	public Sprite checkPointOpen;
	public Sprite checkPointClosed;
	public bool checkPointStatus;
	// Use this for initialization
	void Start () {
		checkPointSpriteRenderer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "Player"){
			checkPointSpriteRenderer.sprite = checkPointOpen;
			checkPointStatus = true;
		}
	}
}
