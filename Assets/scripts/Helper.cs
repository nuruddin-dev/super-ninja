﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helper : MonoBehaviour {

	//Music Play and Stop
	public static void AudioPlay(AudioSource audio){
		audio.Play ();
	}
	public static void AudioStop(AudioSource audio){
		audio.Stop ();
	}
}
