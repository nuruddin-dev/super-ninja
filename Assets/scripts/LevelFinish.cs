﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelFinish : MonoBehaviour {
	public string levelName;
	private SpriteRenderer spriteRenderer;
	public Sprite flagOff;
	public Sprite flagOn;
	public GameObject levelCompletedScreen;
	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		spriteRenderer.sprite = flagOff;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerExit2D(Collider2D collider){
		if(collider.tag == "Player"){
			spriteRenderer.sprite = flagOn;
			StartCoroutine ("newLevel");
		}
	}
	public IEnumerator newLevel(){
		LevelHelper level = FindObjectOfType<LevelHelper> ();
		Helper.AudioStop (level.backgroundMusic);
		yield return new WaitForSeconds (1);
		levelCompletedScreen.SetActive (true);
	}
}
