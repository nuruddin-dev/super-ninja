﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingZombi : MonoBehaviour {
	
	public Transform rightPoint;
	public Transform leftPoint;
	public float moveSpeed;
	private bool movingStatus;
	private Rigidbody2D zombiRigidbody;
	private Animator zombiAnimator;
	public AudioSource zombiWalkingSound;
	//private bool attack;
	//public GameObject player;
	// Use this for initialization
	void Start () {
		zombiRigidbody = GetComponent<Rigidbody2D> ();
		zombiAnimator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(movingStatus && transform.position.x > rightPoint.position.x){
			movingStatus = false;
		}
		if(!movingStatus && transform.position.x < leftPoint.position.x){
			movingStatus = true;
		}
		if(movingStatus){
			transform.localScale = new Vector2 (-1f,1f);
			zombiRigidbody.velocity = new Vector3 (moveSpeed,zombiRigidbody.velocity.y,0f);
		}else{
			transform.localScale = new Vector2 (1f,1f);
			zombiRigidbody.velocity = new Vector3 (-moveSpeed,zombiRigidbody.velocity.y,0f);
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "deadPanel"){
			//Destroy (gameObject);
			if (LevelHelper.soundStatus == 1) {
				Helper.AudioStop (zombiWalkingSound);
			}
			gameObject.SetActive(false);
		}
		if (collider.tag == "Player" && collider.tag != "playerFoot") {
			StartCoroutine ("wait");
		}
	}
	public IEnumerator wait(){
		zombiAnimator.SetBool ("attack", true);
		yield return new WaitForSeconds(1f);
		zombiAnimator.SetBool ("attack", false);
	}
	void OnBecameVisible(){
		if(LevelHelper.soundStatus == 1){
			Helper.AudioPlay (zombiWalkingSound);
		}
	}
	void OnBecameInvisible(){
		if (LevelHelper.soundStatus == 1) {
			Helper.AudioStop (zombiWalkingSound);
		}
	}
}
