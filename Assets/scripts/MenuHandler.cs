﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuHandler : MonoBehaviour {
	
	public AudioSource backgroundMusic;

	public int backgroundMusicStatus;
	public int backgroundSoundStatus;

	public Image musicImage;
	public Sprite musicOn;
	public Sprite musicOff;

	public Image soundImage;
	public Sprite soundOn;
	public Sprite soundOff;

	public GameObject about;
	public GameObject faq;
	public GameObject looding;
	void Awake(){
		if (PlayerPrefs.HasKey ("backgroundMusicStatus")) {
			backgroundMusicStatus = PlayerPrefs.GetInt ("backgroundMusicStatus");
		} else {
			backgroundMusicStatus = 1;
			PlayerPrefs.SetInt ("backgroundMusicStatus",backgroundMusicStatus);
		}

		if (PlayerPrefs.HasKey ("backgroundSoundStatus")) {
			backgroundSoundStatus = PlayerPrefs.GetInt ("backgroundSoundStatus");
		} else {
			backgroundSoundStatus = 1;
			PlayerPrefs.SetInt ("backgroundSoundStatus",backgroundSoundStatus);
		}
	}
	// Use this for initialization
	void Start () {
		looding.SetActive (false);
		if (backgroundMusicStatus==1) {
			//buton change
			musicImage.sprite = musicOn;
			AudioPlay(backgroundMusic);
		} else {
			//buton change
			musicImage.sprite = musicOff;
			AudioStop(backgroundMusic);
		}

		if (backgroundSoundStatus==1) {
			//buton change
			soundImage.sprite = soundOn;
		} else {
			//buton change
			soundImage.sprite = soundOff;
		}
	}

	//Music Button
	public void backgroundMusicButton(){
		backgroundMusicStatus = PlayerPrefs.GetInt ("backgroundMusicStatus");
		if(backgroundMusicStatus==1){
			//buton change
			musicImage.sprite = musicOff;
			//save status
			PlayerPrefs.SetInt ("backgroundMusicStatus",0);
			AudioStop(backgroundMusic);
		}else{
			//buton change
			musicImage.sprite = musicOn;
			//save status
			PlayerPrefs.SetInt ("backgroundMusicStatus",1);
			AudioPlay(backgroundMusic);
		}
	}

	//Sound Button
	public void backgroundSoundButton(){
		backgroundSoundStatus = PlayerPrefs.GetInt ("backgroundSoundStatus");
		if(backgroundSoundStatus==1){
			//buton change
			soundImage.sprite = soundOff;
			//save status
			PlayerPrefs.SetInt ("backgroundSoundStatus",0);
		}else{
			//buton change
			soundImage.sprite = soundOn;
			//save status
			PlayerPrefs.SetInt ("backgroundSoundStatus",1);
		}
	}

	//Open Level
	public void play(string levelName){
		looding.SetActive (true);
		SceneManager.LoadScene (levelName);
	}

	//Exit Application
	public void exit(){
		Application.Quit();
	}


	//Music Play and Stop
	private void AudioPlay(AudioSource audio){
		audio.Play ();
	}
	private void AudioStop(AudioSource audio){
		audio.Stop ();
	}
	public void aboutView(){
		about.SetActive (true);
	}
	public void aboutClose(){
		about.SetActive (false);
	}
	public void closeFAQ(){
		faq.SetActive (false);
	}
	public void viewFAQ(){
		faq.SetActive (true);
	}
}
