﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	public Vector3 respawnPosition;
	private LevelHelper levelHelper;

	// Use this for initialization
	void Start () {
		respawnPosition = transform.position;
		levelHelper = FindObjectOfType<LevelHelper> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D colider){
		if(colider.tag == "deadPanel"){
			if(levelHelper.currentHealth>0f){
				levelHelper.hurtPlayer (2);
				levelHelper.respawn ();
				Debug.Log ("test "+levelHelper.currentHealth);
			}
		}
		if(colider.tag == "checkPoint"){
			respawnPosition = colider.transform.position;
		}
	}
	//for touch object and hold object
	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.tag == "movingPlatform"){
			transform.parent = collision.transform;
		}
	}
	void OnCollisionExit2D(Collision2D collision){
		if(collision.gameObject.tag == "movingPlatform"){
			transform.parent = null;
		}
	}

}
