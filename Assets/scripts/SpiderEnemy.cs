﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderEnemy : MonoBehaviour {
	public float moveSpeed;
	public bool canMove;
	private Rigidbody2D spiderRigidbody;
	public AudioSource spiderWalkingSound;

	// Use this for initialization
	void Start () {
		spiderRigidbody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(canMove){
			spiderRigidbody.velocity = new Vector3 (-moveSpeed,spiderRigidbody.velocity.y,0f);
		}
	}
	void OnBecameVisible(){
		canMove = true;
		if (LevelHelper.soundStatus == 1) {
			Helper.AudioPlay (spiderWalkingSound);
		}
	}
	void OnBecameInvisible(){
		canMove = false;
		Helper.AudioStop (spiderWalkingSound);
	}
	void OnTriggerEnter2D(Collider2D colider){
		if(colider.tag == "deadPanel"){
			//Destroy (gameObject);
			if (LevelHelper.soundStatus == 1) {
				Helper.AudioStop (spiderWalkingSound);
			}
			gameObject.SetActive(false);
		}
	}

	//when reenable its call its self
	void OnEnable(){
		canMove = false;
	}
}
