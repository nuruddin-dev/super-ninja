﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraHealth : MonoBehaviour {
	public int healthToGive;
	private LevelHelper levelHelper;
	public AudioSource healthPickupSound;
	// Use this for initialization
	void Start () {
		levelHelper = FindObjectOfType<LevelHelper> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "Player"){
			levelHelper.addHealth (healthToGive);
			gameObject.SetActive (false);
		}
	}
}
