﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Map : MonoBehaviour {
	public string levelName;
	public int levelStatus;

	public Image levelImage;
	public Sprite lockLevel;
	public Sprite unlockLevel;
	public GameObject loodingScreen;
	void Awake(){
			if (PlayerPrefs.HasKey (levelName)) {
				levelStatus = PlayerPrefs.GetInt (levelName);
			}
	}

	// Use this for initialization
	void Start () {
		loodingScreen.SetActive (false);
		if (levelStatus==1) {
			levelImage.sprite = unlockLevel;
		} else {
			levelImage.sprite = lockLevel;
			//levelImage.GetComponentInParent<Image> ().GetComponentInChildren<Text>().text="";
			gameObject.GetComponentInChildren<Text>().text = "";

		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void loadLevel(){
		if(levelStatus==1){
			loodingScreen.SetActive (true);
			SceneManager.LoadScene (levelName);
		}
	}
}
