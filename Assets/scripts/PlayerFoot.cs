﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFoot : MonoBehaviour {
	public GameObject deathSplosion;
	private Rigidbody2D playerRigidbody;
	private float bounce = 5;
	public AudioSource enemyDeathSound;
	// Use this for initialization
	void Start () {
		playerRigidbody = transform.parent.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider){
		if(collider.tag == "enemy"){
			//Destroy (collider.gameObject);
			collider.gameObject.SetActive(false);
			Instantiate (deathSplosion,collider.transform.position,collider.transform.rotation);
			playerRigidbody.velocity = new Vector3 (playerRigidbody.velocity.x,bounce,0f);
			if(LevelHelper.soundStatus == 1){
				Helper.AudioPlay (enemyDeathSound);
			}

		}
			
	}
}
