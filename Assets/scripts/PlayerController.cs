﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	private Rigidbody2D playerRigidbody;
	private Animator playerAnimator;
	[SerializeField]
	private float playerMovementSpeed;
	[SerializeField]
	private float playerJumpSpeed;
	private bool left;
	private bool right;
	private bool jump;

	public float groundCheckRadius;
	public Transform groundCheck;
	public LayerMask whatIsGround;
	public GameObject playerFoot;
	private LevelHelper levelHelper;
	public bool isGround;

	public float knockBackForce;
	public float knockBackLength;
	private float knockBackCounter;
	public float invinciblityLength;
	private float invinciblityCounter;

	public AudioSource playerJumpSound;
	public AudioSource playerHitSound;

	public Transform firePoint;
	public GameObject ninjaKnife;
	private bool fireKnife;
	public bool playerAlive;
	private bool joyFire;
	// Use this for initialization
	void Start () {
		playerAlive = true;
		playerRigidbody = GetComponent<Rigidbody2D> ();
		playerAnimator = GetComponent<Animator> ();
		levelHelper = FindObjectOfType<LevelHelper> ();
	}

	// Update is called once per frame
	void Update () {
		isGround = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
		//playerRigidbody.velocity = Vector2.left*playerMovementSpeed;
		if (playerAlive) {
			playerAnimator.SetBool ("jump", !isGround);
			playerAnimator.SetFloat ("run", Mathf.Abs (playerRigidbody.velocity.x));
			playerAnimator.SetBool ("throw", fireKnife);
		

		if (knockBackCounter <= 0f) {
			if (right || (Input.GetAxisRaw ("Horizontal") > 0f)) {
				//playerRigidbody.velocity = Vector2.right * playerMovementSpeed;
				transform.localScale = new Vector2 (0.5f, 0.5f);
				playerRigidbody.velocity = new Vector3 (playerMovementSpeed, playerRigidbody.velocity.y, 0f);
			} else if (left || (Input.GetAxisRaw ("Horizontal") < 0f)) {
				//playerRigidbody.velocity = Vector2.left * playerMovementSpeed;
				transform.localScale = new Vector2 (-0.5f, 0.5f);
				playerRigidbody.velocity = new Vector3 (-playerMovementSpeed, playerRigidbody.velocity.y, 0f);
			} else {
				//playerRigidbody.velocity = Vector2.zero;
				playerRigidbody.velocity = new Vector3 (0f, playerRigidbody.velocity.y, 0f);
			}
				if ((jump || Input.GetButtonDown ("Jump") || Input.GetKeyDown(KeyCode.JoystickButton0) 
					|| (Input.GetAxisRaw ("Vertical") > 0f)) && isGround) {
				//playerRigidbody.velocity = new Vector2 (playerRigidbody.velocity.x,playerJumpSpeed);
				if (LevelHelper.soundStatus == 1) {
					Helper.AudioPlay (playerJumpSound);
				}
				playerRigidbody.velocity = new Vector3 (playerRigidbody.velocity.x, playerJumpSpeed, 0f);
				jump = false;
			} 

				if (fireKnife || Input.GetKeyDown (KeyCode.F) || Input.GetKeyDown(KeyCode.JoystickButton1) 
					|| ((Input.GetAxisRaw ("Vertical") < 0f) && !joyFire)) {
				Instantiate (ninjaKnife, firePoint.position, firePoint.rotation);
				fireKnife = false;
					if(!joyFire){
						joyFire = true;
						StartCoroutine ("waitForNextFire");
					}
				
			}
			//levelHelper.invincible = false;
		}
	}
		if (knockBackCounter > 0f) {
			knockBackCounter -= Time.deltaTime;
			if (transform.localScale.x > 0f) {
				playerRigidbody.velocity = new Vector3 (-knockBackForce, knockBackForce, 0f);
			} else {
				playerRigidbody.velocity = new Vector3 (knockBackForce,knockBackForce,0f);
			}

		}
		if(invinciblityCounter>0f){
			invinciblityCounter -= Time.deltaTime;
		}
		if(invinciblityCounter<=0f){
			levelHelper.invincible = false;
		}

		//foot controller
		if(playerRigidbody.velocity.y < 0){
			playerFoot.SetActive (true);
		}else{
			playerFoot.SetActive (false);
		}
	}
	public void knockBack(){
		knockBackCounter = knockBackLength;
		invinciblityCounter = invinciblityLength;
		levelHelper.invincible = true;
		if (LevelHelper.soundStatus == 1) {
			Helper.AudioPlay (playerHitSound);
		}
	}
	public void leftDown(){
		Debug.Log ("leftDown");
		left = true;
		//playerAnimator.SetBool ("run",left);
	}
	public void leftUp(){
		Debug.Log ("leftUp");
		left = false;
		//playerAnimator.SetBool ("run",left);
	}
	public void rightDown(){
		Debug.Log ("rightDown");
		right = true;
		//playerAnimator.SetBool ("run",right);
	}
	public void rightUp(){
		Debug.Log ("rightUp");
		right = false;
		//playerAnimator.SetBool ("run",right);
	}
	public void jumpDown(){
		Debug.Log ("jumpDown");
		jump = true;
	}
	public void jumpUp(){
		Debug.Log ("UpDown");
		jump = false;
	}
	public void fireKnifeDown(){
		Debug.Log ("fireKnifeDown");
		fireKnife = true;
	}
	public void fireKnifeUp(){
		Debug.Log ("fireKnifeUp");
		fireKnife = false;
	}
	public IEnumerator waitForNextFire(){
		yield return new WaitForSeconds (1f);
		joyFire = false;
	}
}
