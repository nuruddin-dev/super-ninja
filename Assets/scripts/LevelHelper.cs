﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelHelper : MonoBehaviour {
	private float waitToRespawn = 2;
	private Player player;
	public GameObject deathSplosion;
	public int totalCoins;
	public Text coinsText;
	public Image healthImage_1;
	public Image healthImage_2;
	public Image healthImage_3;
	public Sprite fullHeart;
	public Sprite halfHeart;
	public Sprite emptyHeart;
	public int maxHealth;
	public int currentHealth;
	public GameObject gameOver;
	public GameObject pauseScreen;
	public GameObject loodingNextScene;

	private bool respawingStatus;

	public ResetObject[] resetToObjects;
	private PlayerController playerController;
	public bool invincible;
	//audio
	public AudioSource playerDeathSound;
	public AudioSource pickupCoinSound;
	public AudioSource pickupHealthSound;
	public AudioSource backgroundMusic;
	public AudioSource gameOverMusic;

	public static int soundStatus;
	public static int musicStatus;

	private AudioSource[] allAudioSources;
	private string[] levelNameArrays = {"level_1","level_2","level_3","level_4","level_5","level_6"};
	void Awake(){
		if (PlayerPrefs.HasKey ("backgroundSoundStatus")) {
			soundStatus = PlayerPrefs.GetInt ("backgroundSoundStatus");
		} else {
			soundStatus = 1;
		}
		if (PlayerPrefs.HasKey ("backgroundMusicStatus")) {
			musicStatus = PlayerPrefs.GetInt ("backgroundMusicStatus");
		} else {
			musicStatus = 1;
		}
	}
	// Use this for initialization
	void Start () {
		loodingNextScene.SetActive (false);
		allAudioSources = FindObjectsOfType (typeof(AudioSource)) as AudioSource[];
		player = FindObjectOfType<Player> ();
		playerController = FindObjectOfType<PlayerController> ();
		coinsText.text = " X " + totalCoins;
		currentHealth = maxHealth;
		resetToObjects = FindObjectsOfType<ResetObject> ();
		if (PlayerPrefs.HasKey ("coins")) {
			totalCoins = PlayerPrefs.GetInt ("coins");
		}
		coinsText.text = " X " + totalCoins;
		//gameOver.SetActive (false);
		if(musicStatus == 1){
			Helper.AudioPlay (backgroundMusic);
		}
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKey(KeyCode.Escape)){
			if(!gameOver.activeSelf){
				pauseCalled ();
			}
		}
		if(currentHealth <=0f && !respawingStatus){
			//check game over
			respawn ();
			respawingStatus = true;
		}
	}

	void OnApplicationPause(){
		if(Application.isMobilePlatform){
			pauseCalled ();
		}

	}
	public void respawn(){
		if (currentHealth > 0f) {
			StartCoroutine ("respawnCoRoutine");
		} else {
			player.gameObject.SetActive (false);
			playerController.playerAlive = false;
			gameOver.SetActive (true);
			if(musicStatus == 1){
				Helper.AudioStop (backgroundMusic);
				Helper.AudioPlay (gameOverMusic);
			}

		}

	}
	public IEnumerator respawnCoRoutine(){
		player.gameObject.SetActive (false);
		Instantiate (deathSplosion,player.transform.position,player.transform.rotation);
		//if sound fx is true

		if(LevelHelper.soundStatus == 1){
			Helper.AudioPlay (playerDeathSound);
		}
		yield return new WaitForSeconds(waitToRespawn);
		//for re-play
		//currentHealth = maxHealth;
		respawingStatus = false;
		updateHealth ();
		player.transform.position = player.respawnPosition;
		player.gameObject.SetActive (true);

		for(int i = 0; i<resetToObjects.Length;i++){
			resetToObjects [i].gameObject.SetActive (true);
			resetToObjects [i].resetObject ();
		}
		//todo
		//hurtPlayer (2);
	}
	public void addToCoins(int coinsToAdd){
		totalCoins += coinsToAdd;
		coinsText.text = " X " + totalCoins;
		PlayerPrefs.SetInt ("coins",totalCoins);
		if(soundStatus == 1){
			Helper.AudioPlay (pickupCoinSound);
		}
	}
	public void hurtPlayer(int damageToTake){
		if (!invincible) {
			Instantiate (deathSplosion,player.transform.position,player.transform.rotation);
			currentHealth -= damageToTake;
			playerController.knockBack ();
			updateHealth ();
		}
	}

	public void addHealth(int addToHealth){
		currentHealth += addToHealth;
		if (currentHealth > maxHealth) {
			currentHealth = maxHealth;
		}
		updateHealth ();
		if(soundStatus == 1){
			Helper.AudioPlay (pickupHealthSound);
		}
	}
	void updateHealth(){
		switch(currentHealth){
		case 6:
			healthImage_1.sprite = fullHeart;
			healthImage_2.sprite = fullHeart;
			healthImage_3.sprite = fullHeart;
			break;
		case 5:
			healthImage_1.sprite = halfHeart;
			healthImage_2.sprite = fullHeart;
			healthImage_3.sprite = fullHeart;
			break;
		case 4:
			healthImage_1.sprite = emptyHeart;
			healthImage_2.sprite = fullHeart;
			healthImage_3.sprite = fullHeart;
			break;
		case 3:
			healthImage_1.sprite = emptyHeart;
			healthImage_2.sprite = halfHeart;
			healthImage_3.sprite = fullHeart;
			break;
		case 2:
			healthImage_1.sprite = emptyHeart;
			healthImage_2.sprite = emptyHeart;
			healthImage_3.sprite = fullHeart;
			break;
		case 1:
			healthImage_1.sprite = emptyHeart;
			healthImage_2.sprite = emptyHeart;
			healthImage_3.sprite = halfHeart;
			break;
		case 0:
			healthImage_1.sprite = emptyHeart;
			healthImage_2.sprite = emptyHeart;
			healthImage_3.sprite = emptyHeart;
			break;
		default:
			healthImage_1.sprite = emptyHeart;
			healthImage_2.sprite = emptyHeart;
			healthImage_3.sprite = emptyHeart;
			break;
		}
	}
	public void exit(){
		Application.Quit ();
	}
	public void goHome(){
		//playerController.playerAlive = true;
		loodingNextScene.SetActive (true);
		if(Time.timeScale == 0){
			Time.timeScale = 1;
		}
		SceneManager.LoadScene ("menu");
	}
	public void goLevelMap(){
		loodingNextScene.SetActive (true);
		SceneManager.LoadScene ("levelMap");
	}
	public void goNewLevel(){
		loodingNextScene.SetActive (true);
		LevelFinish level = FindObjectOfType<LevelFinish> ();
		SceneManager.LoadScene (level.levelName);
	}
	public void rePlay(){
		currentHealth = maxHealth;
		playerController.playerAlive = true;
		//updateHealth ();
		//player.gameObject.SetActive (true);
		gameOver.SetActive (false);
		if(musicStatus == 1){
			Helper.AudioPlay (backgroundMusic);
			Helper.AudioStop (gameOverMusic);
		}
		//respawingStatus = false;
		if (totalCoins > 50) {
			addToCoins (-50 * ((Array.IndexOf(levelNameArrays,SceneManager.GetActiveScene ().name))+1));
		} else {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
		respawn();
	}
	public void resumeGame(){
		playerController.playerAlive = true;
		Time.timeScale = 1;
		pauseScreen.SetActive (false);
		if (PlayerPrefs.HasKey ("backgroundSoundStatus")) {
			int bgSound = PlayerPrefs.GetInt ("backgroundSoundStatus");
			if(bgSound == 1){
				soundStatus = 1;
			}
		}
		if (PlayerPrefs.HasKey ("backgroundMusicStatus")) {
			int bgMusic = PlayerPrefs.GetInt ("backgroundMusicStatus");
			if(bgMusic == 1){
				musicStatus = 1;
				backgroundMusic.Play ();
			}
		}
	}
	void pauseCalled(){
		playerController.playerAlive = false;
		Time.timeScale = 0;
		pauseScreen.SetActive (true);
		if (PlayerPrefs.HasKey ("backgroundSoundStatus")) {
			int bgSound = PlayerPrefs.GetInt ("backgroundSoundStatus");
			if(bgSound == 1){
				soundStatus = 0;
				foreach (AudioSource allAudios in allAudioSources) {
					allAudios.Pause ();
				}
			}
		}
		if (PlayerPrefs.HasKey ("backgroundMusicStatus")) {
			int bgMusic = PlayerPrefs.GetInt ("backgroundMusicStatus");
			if(bgMusic == 1){
				musicStatus = 0;
				backgroundMusic.Pause ();
			}
		}
	}
}
