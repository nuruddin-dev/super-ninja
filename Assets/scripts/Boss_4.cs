﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_4 : MonoBehaviour {
	public Transform rightPoint;
	public Transform leftPoint;
	private Rigidbody2D bossRigidbody;
	private SpriteRenderer spriteRenderer;
	public AudioSource bossWalkingSound;
	public AudioSource bossdeathSound;

	public float moveSpeed;
	private bool movingStatus;
	public float bossSize;
	public int bossHealth;
	private bool bossStatus;
	public string levelName;
	public bool childBossBirth;
	private bool visibility;

	public GameObject finishLevel;
	public GameObject deathBossExp;
	public GameObject hitPoint;
	public GameObject childBoss;


	// Use this for initialization
	void Start () {
		bossRigidbody = GetComponent<Rigidbody2D> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(bossHealth <= 0f && !bossStatus){
			finishLevel.SetActive (true);
			Instantiate (deathBossExp,transform.position,transform.rotation);
			PlayerPrefs.SetInt (levelName,1);
			if (LevelHelper.soundStatus == 1) {
				Helper.AudioPlay (bossdeathSound);
			}
			Destroy (hitPoint);
			Destroy (gameObject);
			bossStatus = true;
		}
		if(movingStatus && transform.position.x > rightPoint.position.x){
			movingStatus = false;
		}
		if(!movingStatus && transform.position.x < leftPoint.position.x){
			movingStatus = true;
		}
		if(movingStatus){
			transform.localScale = new Vector2 (-bossSize,bossSize);
			bossRigidbody.velocity = new Vector3 (moveSpeed,bossRigidbody.velocity.y,0f);
		}else{
			transform.localScale = new Vector2 (bossSize,bossSize);
			bossRigidbody.velocity = new Vector3 (-moveSpeed,bossRigidbody.velocity.y,0f);
			if(!childBossBirth && visibility){
				Instantiate (childBoss,hitPoint.transform.position,hitPoint.transform.rotation);
				StartCoroutine ("childBossBirthTime");
			}
		}
	}
	public void hitBoss(int damage){
		bossHealth -= damage;
		spriteRenderer.color = Color.red;
		StartCoroutine ("colorChange");
	}
	public IEnumerator colorChange(){
		yield return new WaitForSeconds (0.2f);
		spriteRenderer.color = Color.white;
	}
	public IEnumerator childBossBirthTime(){
		childBossBirth = true;
		yield return new WaitForSeconds (5);
		childBossBirth = false;
	}
	void OnBecameVisible(){
		if (LevelHelper.soundStatus == 1) {
			Helper.AudioPlay (bossWalkingSound);
		}
		visibility = true;
	}
}
